import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

//Config
import { ConfigModule } from '@nestjs/config';

//Mongoose
import { MongooseModule } from '@nestjs/mongoose';

//Modules
import RoomModule from './modules/room/room.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: '.dev.env',
        }),
        MongooseModule.forRoot(process.env.MONGO_DB_TEST_URL, {
            autoIndex: true,
        }),
        RoomModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppTestModule {}
