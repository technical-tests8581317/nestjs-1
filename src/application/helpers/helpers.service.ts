import { Injectable, Inject, forwardRef } from '@nestjs/common';

//External Services
import RoomService from '@Modules/room/room.service';
import UserService from '@Modules/user/user.service';

//Handle Error
import handleError from '@Functions/handleError';

@Injectable()
export default class HelpersService {
    constructor(
        @Inject(forwardRef(() => RoomService))
        private readonly roomService: RoomService,
        @Inject(forwardRef(() => UserService))
        private readonly userService: UserService
    ) {}

    async generatePublicId(model = '', public_id = '') {
        try {
            let newpublicid: string = public_id;

            //There is a model and name, let's generate a slug from the public_id
            if (public_id && model) {
                //First generate the slug
                newpublicid = this.generateSlug(newpublicid);

                //Then check if it doesn't already exist
                let doesExist = await this.checkElement(
                    model,
                    'public_id',
                    newpublicid
                );
                let newId;
                while (doesExist) {
                    if (!newId) {
                        newId = 1;
                    } else {
                        newpublicid = newpublicid.substr(
                            0,
                            newpublicid.lastIndexOf('-')
                        );
                        newId++;
                    }
                    newpublicid += '-' + newId;
                    doesExist = await this.checkElement(
                        model,
                        'public_id',
                        newpublicid
                    );
                }
            }

            //Generate a random slug
            else {
                newpublicid = this.generateRandomId(12);
            }

            return newpublicid;
        } catch (e) {
            handleError(e);
        }
    }

    //Capitalize First Letter
    capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };

    //Generate a Random ID
    generateRandomId = (length) => {
        let text = '';
        const possible =
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        for (let i = 0; i < length; i++)
            text += possible.charAt(
                Math.floor(Math.random() * possible.length)
            );

        return text;
    };

    //Remove Escape string
    unEscape = (htmlStr = '') => {
        htmlStr = htmlStr.replace(/&lt;/g, '<');
        htmlStr = htmlStr.replace(/&gt;/g, '>');
        htmlStr = htmlStr.replace(/&quot;/g, '"');
        htmlStr = htmlStr.replace(/&#39;/g, "'");
        htmlStr = htmlStr.replace(/&amp;/g, '&');
        return htmlStr;
    };

    //Check Element
    checkElement = async (model, field, value) => {
        const params: any = {};
        params[field] = value;
        let count = 0;
        if (model === 'Room') {
            count = <number>await this.roomService.count(params);
        } else if (model === 'User') {
            count = <number>await this.userService.count(params);
        }
        if (count > 0) return true;
        else return false;
    };

    //Generate Slug
    generateSlug = (text) => {
        return text
            .normalize('NFD')
            .replace(/[^a-z0-9 -]^[\u4e00-\u9fa5]/g, '') // remove all that not are a letter, a number, and are not a chinese word
            .toLowerCase() // Convert the string to lowercase letters
            .replace(/\p{Diacritic}/gu, '') // Replace accents
            .replace(/[^\w\-]+/g, '-') // Remove all non-word chars
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/-+/g, '-')
            .replace('-?', '')
            .replace('?', '')
            .replace(/\-$/g, ''); // Remove trailing -
    };

    //Get the Id From The Public ID
    getIdFromPublicId = async (public_id, model = '') => {
        const element = await this.generateSlug(public_id);
        if (element) return element._id;
        else return false;
    };
}
