import { Injectable, Inject, forwardRef } from '@nestjs/common';

//Internal Services
import GenerateSlug from './generateSlug.helpers';

@Injectable()
export default class HelpersService {
  constructor(private readonly generateSlug: GenerateSlug) {}

  //Get the Id From The Public ID
  run = async (public_id, model = '') => {
    let element;

    element = await this.generateSlug.run(public_id);
    if (element) return element._id;
    else return false;
  };
}
