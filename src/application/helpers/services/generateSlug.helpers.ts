import { Injectable } from '@nestjs/common';

@Injectable()
export default class GenerateSlug {
  //Generate Slug
  run = (text) => {
    return text
      .normalize('NFD')
      .replace(/[^a-z0-9 -]^[\u4e00-\u9fa5]/g, '') // remove all that not are a letter, a number, and are not a chinese word
      .toLowerCase() // Convert the string to lowercase letters
      .replace(/\p{Diacritic}/gu, '') // Replace accents
      .replace(/[^\w\-]+/g, '-') // Remove all non-word chars
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(/-+/g, '-')
      .replace('-?', '')
      .replace('?', '')
      .replace(/\-$/g, ''); // Remove trailing -
  };
}
