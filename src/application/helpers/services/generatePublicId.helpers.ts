import { Injectable, Inject, forwardRef } from '@nestjs/common';

//Internal Services
import CheckElement from './checkElement.helpers';
import GenerateSlug from './generateSlug.helpers';
@Injectable()
export default class HelpersService {
  constructor(
    private readonly checkElement: CheckElement,
    private readonly generateSlug: GenerateSlug,
  ) {}

  async run(model, public_id) {
    let newpublicid: string = public_id;

    //There is a model and name, let's generate a slug from the public_id
    if (public_id && model) {
      //First generate the slug
      newpublicid = this.generateSlug.run(newpublicid);

      //Then check if it doesn't already exist
      let doesExist = await this.checkElement.run(
        model,
        'public_id',
        newpublicid,
      );
      let newId;
      while (doesExist) {
        if (!newId) {
          newId = 1;
        } else {
          newpublicid = newpublicid.substr(0, newpublicid.lastIndexOf('-'));
          newId++;
        }
        newpublicid += '-' + newId;
        doesExist = await this.checkElement.run(
          model,
          'public_id',
          newpublicid,
        );
      }
    }

    //Generate a random slug
    else {
      newpublicid = this.generateRandomId(12);
    }

    return newpublicid;
  }

  //Capitalize First Letter
  capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  //Generate a Random ID
  generateRandomId = (length) => {
    let text = '';
    const possible =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  };
}
