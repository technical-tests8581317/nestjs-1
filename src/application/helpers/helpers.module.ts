import { Module, forwardRef } from '@nestjs/common';

//Components
import HelpersService from './helpers.service';
import GeneratePublicId from './services/generatePublicId.helpers';
import GenerateSlug from './services/generateSlug.helpers';
import CheckElementService from './services/checkElement.helpers';
import GetIdFromPublicId from './services/getIdFromPublicId.helpers';

//Modules
import UserModule from '@Modules/user/user.module';
import RoomModule from '@Modules/room/room.module';

@Module({
    imports: [UserModule, forwardRef(() => RoomModule)],
    providers: [
        HelpersService,
        GeneratePublicId,
        CheckElementService,
        GetIdFromPublicId,
        GenerateSlug,
    ],
    exports: [
        HelpersService,
        GeneratePublicId,
        CheckElementService,
        GetIdFromPublicId,
        GenerateSlug,
    ],
})
export default class HelpersModule {}
