import { HttpException, HttpStatus } from '@nestjs/common';
import mongoose from 'mongoose';

const handleError = (
    error: any,
    defaultMessage = 'Something wrong happened !'
): Error => {
    if (error instanceof Error) {
        //Validation Error
        if (error instanceof mongoose.Error.ValidationError) {
            throw new HttpException(
                error.message,
                HttpStatus.UNPROCESSABLE_ENTITY
            );
        } else if (error instanceof HttpException) {
            //HttpException shows an explicit message error
            throw new HttpException(error.message, error.getStatus());
        }
        //Others kind of errors which are not explicit should return a generic message
        else {
            throw new HttpException(defaultMessage, HttpStatus.BAD_REQUEST);
        }
    }
    //All other errors should return a generic message
    else {
        throw new HttpException(defaultMessage, HttpStatus.BAD_REQUEST);
    }
};

export default handleError;
