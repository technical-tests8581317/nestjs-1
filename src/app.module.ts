import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { APP_INTERCEPTOR } from '@nestjs/core';

//Config
import { ConfigModule } from '@nestjs/config';

//Mongoose
import { MongooseModule } from '@nestjs/mongoose';

//Modules
import UserModule from './modules/user/user.module';
import RoomModule from './modules/room/room.module';
import MessageModule from './modules/message/message.module';

//Interceptors
import FormatResponseInterceptor from '@Interceptors/formatResponse.interceptor';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: '.dev.env',
        }),
        MongooseModule.forRoot(process.env.MONGO_DB_URL, {
            autoIndex: true,
        }),
        UserModule,
        RoomModule,
        MessageModule,
    ],
    controllers: [AppController],
    providers: [
        AppService,
        {
            provide: APP_INTERCEPTOR,
            useClass: FormatResponseInterceptor,
        },
    ],
})
export class AppModule {}
