//NestJs
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';

//Mongoose
import { InjectModel } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Model, Types } from 'mongoose';

//Schema
import { Message } from './message.schema';

//DTO
import { MessageInput, MessageQuery } from './message.dto';

//Handle Error
import handleError from '@Functions/handleError';

@Injectable()
export default class MessageService {
    constructor(
        @InjectModel(Message.name)
        private messageModel: Model<Message>
    ) {}

    //Add Message
    async addMessage(inputs: MessageInput): Promise<boolean | Error> {
        try {
            //Transform Data
            inputs.room_id = new mongoose.Types.ObjectId(inputs.room_id);
            inputs.sender_id = new mongoose.Types.ObjectId(inputs.sender_id);
            //Build Data
            const data: Message = {
                _id: new mongoose.Types.ObjectId(),
                ...inputs,
            };
            const newMessage = <Message>(
                await new this.messageModel(data).save()
            );
            if (newMessage) return true;
            else throw new Error();
        } catch (error) {
            handleError(error, 'Cannot Create A Message');
        }
    }

    //Get Messages
    async getMessages(query: MessageQuery): Promise<Message[] | Error> {
        try {
            if (query.room_id) {
                query.room_id = new Types.ObjectId(query.room_id);
            }
            const messages = <Message[]>await this.messageModel
                .find(query, {
                    _id: 0,
                    content: 1,
                    sender_id: 1,
                    createdAt: 1,
                })
                .sort({ createdAt: -1 });
            if (!messages) {
                throw new HttpException(
                    'No Messages found in this room',
                    HttpStatus.NO_CONTENT
                );
            }
            return messages;
        } catch (error) {
            handleError(error, 'Cannot Fetch Messages');
        }
    }
}
