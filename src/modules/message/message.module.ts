import { Module } from '@nestjs/common';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { Message, MessageSchema } from './message.schema';

//Service
import MessageService from './message.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Message.name, schema: MessageSchema },
        ]),
    ],
    providers: [MessageService],
    exports: [MessageService],
    controllers: [],
})
export default class MessageModule {}
