import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types } from 'mongoose';

export type MessageDocument = HydratedDocument<Message>;

@Schema()
export class Message {
    @Prop()
    _id: Types.ObjectId;
    @Prop({
        required: [true, 'A message id is required !'],
    })
    room_id: Types.ObjectId;
    @Prop({
        required: [true, 'A sender id is required !'],
    })
    sender_id: Types.ObjectId;
    @Prop({
        required: [true, 'A content is required !'],
    })
    content: string;
    @Prop({
        required: false,
        default: new Date(),
    })
    createdAt?: Date;
}
export const MessageSchema = SchemaFactory.createForClass(Message);
