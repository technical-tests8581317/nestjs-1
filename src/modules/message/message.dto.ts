import { Types } from 'mongoose';
import {
    IsNotEmpty,
    IsString,
    IsMongoId,
    IsDate,
    IsOptional,
} from 'class-validator';
class MessageInput {
    @IsMongoId()
    @IsNotEmpty()
    room_id: Types.ObjectId;

    @IsMongoId()
    @IsNotEmpty()
    sender_id: Types.ObjectId;

    @IsString()
    @IsNotEmpty()
    content: string;

    @IsDate()
    @IsOptional()
    createdAt?: Date;
}

class MessageQuery {
    @IsMongoId()
    @IsNotEmpty()
    room_id: Types.ObjectId;
}

export { MessageInput, MessageQuery };
