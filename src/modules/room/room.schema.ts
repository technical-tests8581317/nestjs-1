import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types } from 'mongoose';

export type RoomDocument = HydratedDocument<Room>;

@Schema()
export class Room {
    @Prop()
    _id: Types.ObjectId;
    @Prop({
        required: [true, 'A public id is required !'],
        unique: true,
    })
    public_id?: string;
    @Prop({
        required: [true, 'A name is required !'],
        unique: true,
        validate: {
            validator: (v) => {
                return v.length >= 3 ?? true;
            },
            message: () =>
                'The name of the room must be at least 3 characters length',
        },
    })
    name: string;
    @Prop({
        required: false,
        unique: false,
    })
    users?: [{ type: Types.ObjectId; ref: 'User' }];
}
export const RoomSchema = SchemaFactory.createForClass(Room);
