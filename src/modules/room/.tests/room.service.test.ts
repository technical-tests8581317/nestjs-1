import mongoose from 'mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

//App Test
import { AppTestModule } from '../../../appTest.module';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { Room, RoomSchema } from '../room.schema';

//Room
import RoomService from '../room.service';

//Mongoose
import { Types } from 'mongoose';

//Modules
import HelpersModule from '../../../application/helpers/helpers.module';
import MessageModule from '../../message/message.module';

describe('RoomService', () => {
    let app: INestApplication;
    let service: RoomService;

    const roomTest: Room = {
        _id: new Types.ObjectId(),
        name: 'Bruce Springsteen',
    };

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                AppTestModule,
                MongooseModule.forFeature([
                    { name: Room.name, schema: RoomSchema },
                ]),
                MessageModule,
                HelpersModule,
            ],
            exports: [RoomService],
            providers: [RoomService],
        }).compile();
        app = module.createNestApplication();
        service = app.get<RoomService>(RoomService);
        await app.init();
    });

    afterAll(async () => {
        mongoose.connection.close();
        await app.close();
    });

    it('Components should be defined', () => {
        expect(app).toBeDefined();
        expect(service).toBeDefined();
    });

    test('Service : Add Room', async () => {
        const roomAlreadyExists = await service.addRoom(roomTest);
        if (!roomAlreadyExists) {
            const result = <Room>await service.addRoom(roomTest);
            roomTest.public_id = result.public_id;
            expect(result).toBe(true);
        }
    });

    test('Service : Get Room By Id', async () => {
        const result = await service.getRoomById(roomTest._id);
        expect(typeof result).toBe('object');
        expect(result).toHaveProperty('_id');
    });

    test('Service : Update Room', async () => {
        const user = new Types.ObjectId();
        const updatedRoom = await service.updateRoom(roomTest._id, {
            user,
        });
        expect(updatedRoom).toBe(true);
    });

    test('Service : Count Room', async () => {
        const count = await service.count({ public_id: 'bruce-springsteen' });
        expect(count).toBeGreaterThan(0);
    });

    test('Service : Delete Room', async () => {
        const result = await service.deleteRoom({
            public_id: 'bruce-springsteen',
        });
        expect(result).toBe(true);
    });
});
