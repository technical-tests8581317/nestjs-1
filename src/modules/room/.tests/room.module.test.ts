import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import mongoose from 'mongoose';

//App Test
import { AppTestModule } from '../../../appTest.module';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { Room, RoomSchema } from '../room.schema';

//Controller
import RemoteController from '../room.controller';

//Service
import RoomService from '../room.service';

//Modules
import HelpersModule from '../../../application/helpers/helpers.module';
import MessageModule from '../../message/message.module';

describe('RoomModule', () => {
    let app: INestApplication;
    let controller: RemoteController;
    let service: RoomService;
    let data;
    const public_id = 'david-bowie';

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                AppTestModule,
                MongooseModule.forFeature([
                    { name: Room.name, schema: RoomSchema },
                ]),
                MessageModule,
                HelpersModule,
            ],
            exports: [RoomService],
            providers: [RoomService],
            controllers: [RemoteController],
        }).compile();
        app = module.createNestApplication();
        await app.init();
        controller = module.get<RemoteController>(RemoteController);
        service = module.get<RoomService>(RoomService);
    });
    afterAll(async () => {
        mongoose.connection.close();
        await app.close();
    });

    beforeEach(async () => {});

    it('Components should be defined', () => {
        expect(app).toBeDefined();
        expect(controller).toBeDefined();
        expect(service).toBeDefined();
    });
});
