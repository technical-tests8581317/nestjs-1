import { Test } from '@nestjs/testing';
import RoomController from '../room.controller';
import RoomService from '../room.service';

//App Test
import { AppTestModule } from '../../../appTest.module';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { Room, RoomSchema } from '../room.schema';
import { User } from '../../user/user.schema';
import { Message } from '../../message/message.schema';
//Mongoose
import { Types } from 'mongoose';

//Modules
import HelpersModule from '../../../application/helpers/helpers.module';
import MessageModule from '../../message/message.module';
import RoomModule from '../../room/room.module';

describe('RoomController', () => {
    let controller: RoomController;
    let service: RoomService;
    let data: Room;

    const roomTest: Room = {
        _id: new Types.ObjectId(),
        name: 'David Bowie',
    };
    const userTest: User = {
        _id: new Types.ObjectId(),
        public_id: 'jerome-charriaud',
        name: 'Jerome Charriaud',
    };

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                AppTestModule,
                MongooseModule.forFeature([
                    { name: Room.name, schema: RoomSchema },
                ]),
                MessageModule,
                HelpersModule,
            ],
            controllers: [RoomController],
            providers: [RoomService],
        }).compile();

        service = moduleRef.get<RoomService>(RoomService);
        controller = moduleRef.get<RoomController>(RoomController);
        data = <Room>await controller.createRoom(roomTest);
    });

    it('Components should be defined', () => {
        expect(controller).toBeDefined();
        expect(service).toBeDefined();
    });

    test('Controller: A Room should have been added', async () => {
        expect(typeof data).toBe('object');
        expect(data._id).not.toBeNull();
    });

    test('Controller: Add a User to a Room', async () => {
        const addUser = await controller.addUser(
            { _id: roomTest._id },
            { user: userTest._id }
        );
        expect(addUser).toBe(true);
    });

    test('Controller: Add a Message to a Room', async () => {
        const addMessage = await controller.addMessage(
            { _id: roomTest._id },
            { sender_id: userTest._id, content: 'Hello World !' }
        );
        expect(addMessage).toBe(true);
    });

    test('Controller: Get Messages from a Room', async () => {
        const messages = <Message[]>(
            await controller.getMessages({ _id: roomTest._id })
        );
        expect(messages.length).toBeGreaterThan(0);
    });

    afterAll(async () => {
        await service.deleteRoom({
            public_id: data.public_id,
        });
    });
});
