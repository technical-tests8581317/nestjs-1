//NestJs
import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';

//Mongoose
import { InjectModel } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Model, Types } from 'mongoose';
//Schema
import { Room } from './room.schema';

//DTO
import { RoomInput, RoomUpdateInput, RoomArgs } from './room.dto';

//Helpers
import HelperServices from '@Application/helpers/helpers.service';

//Handle Error
import handleError from '@Functions/handleError';

@Injectable()
export default class RoomService {
    constructor(
        @InjectModel(Room.name)
        private roomModel: Model<Room>,
        @Inject(HelperServices)
        private helperServices: HelperServices
    ) {}
    async addRoom(inputs: RoomInput): Promise<Room | Error> {
        try {
            //Generate Public Id
            const roomName: string = inputs.name;
            const newPublicId = await this.helperServices.generatePublicId(
                Room.name,
                roomName
            );

            //Build Data
            const data: Room = {
                _id: new mongoose.Types.ObjectId(),
                public_id: newPublicId,
                ...inputs,
            };
            const newRoom = <Room>await new this.roomModel(data).save();
            if (newRoom) return newRoom;
            else throw new Error();
        } catch (error) {
            handleError(error, 'Cannot Create A Room');
        }
    }

    //Get Messages
    async getRoomById(_id: Types.ObjectId): Promise<Room | Error> {
        try {
            const room = <Room>(
                await this.roomModel.findById(new Types.ObjectId(_id)).exec()
            );
            if (!room) {
                throw new HttpException(
                    'No Room has been found',
                    HttpStatus.NO_CONTENT
                );
            }
            return room;
        } catch (error) {
            handleError(error, 'No Room Has been found');
        }
    }

    async updateRoom(
        _id: Types.ObjectId,
        inputs: RoomUpdateInput
    ): Promise<boolean | Error> {
        try {
            const room = <Room>await this.getRoomById(_id);

            //Merge Users
            const newUsers = [...new Set([...room.users, inputs.user])];

            //Update Room
            const updatedRoom = await this.roomModel.updateOne(
                { _id: new Types.ObjectId(_id) },
                { users: newUsers, name: room.name }
            );
            if (updatedRoom.matchedCount) return true;
            else
                throw new HttpException(
                    'Room cannot be found',
                    HttpStatus.NOT_FOUND
                );
        } catch (error) {
            handleError(error, 'Cannot Update Room');
        }
    }

    async count(data: RoomArgs): Promise<number | Error> {
        try {
            return await this.roomModel.count(data);
        } catch (error) {
            handleError(error, 'Cannot Count Rooms');
        }
    }

    //Delete Room
    async deleteRoom(data: RoomArgs): Promise<boolean | undefined> {
        try {
            await this.roomModel.deleteOne(data);
            return true;
        } catch (e) {
            throw new Error(e);
        }
    }
}
