//NestJs
import {
    Body,
    Controller,
    Post,
    Get,
    Param,
    HttpException,
    HttpStatus,
} from '@nestjs/common';

//Model
import { Message } from '@Modules/message/message.schema';

//Schema
import { Room } from './room.schema';

//Service
import RoomService from './room.service';
import MessageService from '@Modules/message/message.service';

//DTO
import {
    RoomInput,
    RoomUpdateInput,
    RoomParams,
    RoomAddMessageInput,
} from './room.dto';

//Handle Error
import handleError from '@Functions/handleError';

@Controller('room')
export default class RoomController {
    constructor(
        private readonly roomService: RoomService,
        private readonly messageService: MessageService
    ) {}

    //Add a Room
    @Post('add')
    async createRoom(@Body() input: RoomInput): Promise<Room | Error> {
        try {
            const room = await this.roomService.addRoom(input);
            return room;
        } catch (error) {
            handleError(error, 'Cannot Create Room');
        }
    }

    //Add User To A Room
    @Post(':_id/addUser')
    async addUser(
        @Param()
        params: RoomParams,
        @Body() body: RoomUpdateInput
    ): Promise<boolean | Error> {
        try {
            if (!body.user) {
                throw new HttpException(
                    'User Id is missing !',
                    HttpStatus.UNPROCESSABLE_ENTITY
                );
            }
            return await this.roomService.updateRoom(params._id, body);
        } catch (error) {
            handleError(error, 'Cannot Add User to the Room');
        }
    }

    //Add Message To A Room
    @Post(':_id/addMessage')
    async addMessage(
        @Param()
        params: RoomParams,
        @Body() body: RoomAddMessageInput
    ): Promise<boolean | Error> {
        try {
            return await this.messageService.addMessage({
                room_id: params._id,
                ...body,
            });
        } catch (error) {
            handleError(error, 'Cannot Add Message to the Room');
        }
    }

    //Get Messages Of A Room
    @Get(':_id/getMessages')
    async getMessages(
        @Param()
        params: RoomParams
    ): Promise<Message[] | Error> {
        try {
            const messages = <Message[]>await this.messageService.getMessages({
                room_id: params._id,
            });
            return messages;
        } catch (error) {
            handleError(error, 'Cannot Add Message to the Room');
        }
    }
}
