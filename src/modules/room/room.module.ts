import { Module } from '@nestjs/common';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { Room, RoomSchema } from './room.schema';

//Controller
import RemoteController from './room.controller';

//Service
import RoomService from './room.service';

//Modules
import HelpersModule from '@Application/helpers/helpers.module';
import MessageModule from '../message/message.module';
@Module({
    imports: [
        MongooseModule.forFeature([{ name: Room.name, schema: RoomSchema }]),
        MessageModule,
        HelpersModule,
    ],
    exports: [RoomService],
    providers: [RoomService],
    controllers: [RemoteController],
})
export default class RoomModule {}
