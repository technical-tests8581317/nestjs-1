import { Types } from 'mongoose';
import {
    IsArray,
    IsNotEmpty,
    IsString,
    IsOptional,
    IsMongoId,
} from 'class-validator';
class RoomInput {
    @IsString()
    name: string;

    @IsString()
    @IsOptional()
    public_id?: string;

    @IsArray()
    @IsOptional()
    users?: [{ type: Types.ObjectId; ref: 'User' }];
}

class RoomUpdateInput {
    @IsString()
    @IsOptional()
    user: Types.ObjectId;
}

class RoomArgs {
    @IsString()
    @IsNotEmpty()
    public_id?: string;
}

class RoomParams {
    @IsMongoId()
    @IsNotEmpty()
    _id: Types.ObjectId;
}

class RoomAddMessageInput {
    @IsMongoId()
    @IsNotEmpty()
    sender_id: Types.ObjectId;
    @IsString()
    @IsNotEmpty()
    content: string;
}

export {
    RoomInput,
    RoomUpdateInput,
    RoomParams,
    RoomArgs,
    RoomAddMessageInput,
};
