import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types } from 'mongoose';

export type UserDocument = HydratedDocument<User>;

@Schema()
export class User {
    @Prop()
    _id: Types.ObjectId;
    @Prop({
        required: [true, 'A public id is required !'],
        unique: true,
    })
    public_id: string;
    @Prop({
        required: [true, 'A name is required !'],
        unique: true,
        validate: {
            validator: (v) => {
                return v.length >= 6 ?? true;
            },
            message: () =>
                'The name of the user must be at least 6 characters length',
        },
    })
    name: string;
}
export const UserSchema = SchemaFactory.createForClass(User);
