//NestJs
import { Injectable } from '@nestjs/common';

//Mongoose
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

//Schema
import { User } from './user.schema';

//Handle Error
import handleError from '@Functions/handleError';

@Injectable()
export default class UserService {
    constructor(
        @InjectModel(User.name)
        private userModel: Model<User>
    ) {}

    async count(public_id: string): Promise<number | Error> {
        try {
            return await this.userModel.count({ public_id });
        } catch (error) {
            handleError(error, 'Cannot Count Users');
        }
    }
}
