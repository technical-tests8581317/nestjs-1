import { Module } from '@nestjs/common';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './user.schema';

//Service
import UserService from './user.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    ],
    controllers: [],
    providers: [UserService],
    exports: [UserService],
})
export default class UserModule {}
