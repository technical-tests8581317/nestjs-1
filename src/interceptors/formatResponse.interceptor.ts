import {
    CallHandler,
    ExecutionContext,
    Injectable,
    NestInterceptor,
} from '@nestjs/common';
import { map, Observable } from 'rxjs';

interface ResponseData {
    ok: boolean;
    statusCode: number;
    data?: any;
}

@Injectable()
export default class FormatResponseInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        const ctx = context.switchToHttp();
        const response = ctx.getResponse();

        return next.handle().pipe(
            map((value) => {
                value = value ? value : [];
                const data: ResponseData = {
                    ok: true,
                    statusCode: response.statusCode,
                };
                if (value) {
                    data.data = value;
                }
                return data;
            })
        );
    }
}
