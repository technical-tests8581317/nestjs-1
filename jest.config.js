// jest.config.js
module.exports = {
    rootDir: '',
    roots: ['<rootDir>/src'],
    moduleFileExtensions: ['js', 'json', 'ts'],
    modulePaths: ['<rootDir>/src/modules.'],
    // setupFiles: ['<rootDir>/.test/setEnvVars.ts'],
    moduleNameMapper: {
        '^src/(.*)$': '<rootDir>/src/$1',
        '@Application/(.*)$': '<rootDir>/src/application/$1',
        '@Modules/(.*)$': '<rootDir>/src/modules/$1',
        '@Api/(.*)$': '<rootDir>/src/api/$1',
        '@Functions/(.*)$': '<rootDir>/src/functions/$1',
        '@Infrastructure/(.*)$': '<rootDir>/src/infrastructure/$1',
        '@Interceptors/(.*)$': '<rootDir>/src/interceptors/$1',
        '@Application/(.*)$': '<rootDir>/src/application/$1',
        '@Entities/(.*)$': '<rootDir>/src/entities/$1',
        '@Helpers/(.*)$': '<rootDir>/src/application/helpers/$1',
        '@Decorators/(.*)$': '<rootDir>/src/application/decorators/$1',
        '@Tools/(.*)$': '<rootDir>/src/application/tools/$1',
    },
    testRegex: '.*\\.(test|spec)\\.ts$',
    transform: {
        '^.+\\.(t|j)s$': 'ts-jest',
    },
    collectCoverageFrom: ['**/*.(t|j)s'],
    coverageDirectory: '../coverage',
    testEnvironment: 'node',
};
