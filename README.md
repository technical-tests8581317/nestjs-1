<h1>API Challenge</h1>

<h2>Create a Chat Room API with Nest.Js</h2>

This Channel can be modeled much like a Chat room, and as we are an API first company,
we would like to see how you would model a small API to model a chat like behavior with several endpoints:

-   Create a room
-   Add a user to a room
-   Send a message to a room
-   Get latest messages from a room

Code Challenge Requisites

<h3>Code Challenge Requisites</h3>

-   Automated tests should be added to verify the correct behavior of the API.
-   Use a database of your choice (or no database at all if you prefer).

<h3>Evaluation</h3>

-   Usage of Clean Code and Clean Architecture.
-   Testing.
-   Adherence to best practices.
-   Readability.
